#Takes lib_def as argument and lists the .o files
funct1() {
	
	echo "Searching object files in $1" >>LogFile.txt
	s=`grep $interestedObject $1`
	if [ ! -z "$s" ]
	then
		completename="$completename -> $1 -> end"
		echo $completename>>Found.txt
		completename="Begin1";
#	else
#		s=`grep ^library $1`
#		if [ -z "$s" ]
#		then
#			completename="Begin";
#		fi
	fi
}

#Takes .a as input string and converts to lib_def and calls main
funct2() {
	#archiveName=`echo $1 | cut -d ' ' -f2 | cut -d '.' -f1`
	if [ "$1" != "library" ]
	then 
		return
	fi
	archiveName=`echo $2 | cut -d '.' -f1`
	libDefName="$archiveName.lib_def"
	completename="$completename -> $libDefName"
	echo "$completename"
	alreadyprocessed=`grep $libDefName $lookup`
	if [ -z "$alreadyprocessed" ]
	then
                echo "$libDefName" >>Lookupfiles.txt
		echo "From function 2 input and output $1, $2, $archiveName, $libDefName" >> LogFile.txt
		if [ -f $libDefName ]
		then
			main2 $libDefName
		fi
	fi
}
main2() {

        funct1 $1;
        while read line
        do
		echo "From second read line: $line" >> LogFile.txt
                funct2 $line
        done < $1 
}

# takes lib_def as argument calls func1 to get the object names. then calls funct2 to get the lib_def names of .a in that file.
main() {
	
	funct1 $1;
	while read line 
	do
		completename="Start"
		echo "From Main read line: $line" >> LogFile.txt
		funct2 $line
	done < $1 
}
completename=""
lookup=Lookupfiles.txt
if [ -f $lookup ]
then
	rm $lookup
fi
touch "Lookupfiles.txt"
if [ -f "LogFile.txt" ]
then
	rm "LogFile.txt"
fi
if [ -f "Found.txt" ]
then
	rm "Found.txt"
fi	
interestedObject=$2
main $1
